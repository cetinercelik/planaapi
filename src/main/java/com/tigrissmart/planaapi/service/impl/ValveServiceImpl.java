package com.tigrissmart.planaapi.service.impl;

import com.tigrissmart.planaapi.dao.ValveRepository;
import com.tigrissmart.planaapi.dao.entity.Valve;
import com.tigrissmart.planaapi.dto.ValveDto;
import com.tigrissmart.planaapi.service.ValveService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ValveServiceImpl implements ValveService {
    @Autowired
    private ValveRepository valveRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ValveDto save(ValveDto valveDto) {
        Valve vl=modelMapper.map(valveDto,Valve.class);//valveDto dan gelen değerleri Valve Classına map etme işlemi yapıyor
        vl=valveRepository.save(vl);//Map ettiğimiz değerleri kaydediyoruz
        valveDto.setId(vl.getId());
        return valveDto;
    }

    @Override
    public ValveDto getById(Long id) {
        Valve vl=valveRepository.getById(id);
        return  modelMapper.map(vl,ValveDto.class);
    }

    @Override
    public List<ValveDto> getAll() {
        List<Valve> data=valveRepository.findAll();
        return Arrays.asList(modelMapper.map(data,ValveDto[].class));
    }

    @Override
    public Boolean delete(Long id) {
        valveRepository.deleteById(id);
        return true;
    }

    @Override
    public ValveDto update(Long id, ValveDto valveDto) {
        Valve valveDb=valveRepository.getById(id);
        if (valveDb==null)
            throw new IllegalArgumentException("Note does not exist id : "+id);
        valveDb.setName(valveDto.getName());
        valveDb.setLocation(valveDto.getLocation());
        valveDb.setStatus(valveDto.getStatus());
        valveDb.setModel(valveDto.getModel());
        valveRepository.save(valveDb);

        return modelMapper.map(valveDb,ValveDto.class);
    }
}
