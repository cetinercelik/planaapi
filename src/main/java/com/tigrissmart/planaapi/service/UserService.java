package com.tigrissmart.planaapi.service;

import com.tigrissmart.planaapi.dto.UserDto;
import com.tigrissmart.planaapi.util.TPage;
import org.springframework.data.domain.Pageable;


public interface UserService {

    UserDto save(UserDto user);
    UserDto getById(Long id);
    TPage<UserDto> getAllPageable(Pageable pageable);
    UserDto getByUsername(String username);
}
