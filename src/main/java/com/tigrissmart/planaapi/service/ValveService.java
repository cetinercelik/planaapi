package com.tigrissmart.planaapi.service;

import com.tigrissmart.planaapi.dto.ValveDto;

import java.util.List;

public interface ValveService {
    ValveDto save(ValveDto valveDto);
    ValveDto getById(Long id);
    List<ValveDto> getAll();
    Boolean delete(Long id);
    ValveDto update(Long id,ValveDto valveDto);
}
