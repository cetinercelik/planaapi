package com.tigrissmart.planaapi.dao;

import com.tigrissmart.planaapi.dao.entity.Valve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface ValveRepository extends JpaRepository<Valve,Long> {
}
