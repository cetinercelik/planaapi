package com.tigrissmart.planaapi.dao.entity;

import com.tigrissmart.planaapi.dao.entity.common.BaseEntity;
import lombok.*;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Builder
@Table(name = "Valves")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Valve extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id",nullable = false)
    private Long id;

    @Column(name="Name")
    private String name;

    @Column(name = "Location")
    private String location;

    @Column(name = "Model")
    private String model;

    @Column(name = "Status")
    private Boolean status;



}
