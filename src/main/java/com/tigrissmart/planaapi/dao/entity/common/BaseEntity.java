package com.tigrissmart.planaapi.dao.entity.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@SuppressWarnings("serial")
@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {
    @CreatedBy
    @Column(name ="CreatedBy")
    private String createdBy="anonim";

    @CreatedDate
    @Column(name = "CreateAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @LastModifiedBy
    @Column(name = "ModifiedBy")
    private String modifiedBy="anonim";

    @LastModifiedDate
    @Column(name = "ModifiedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
}
