package com.tigrissmart.planaapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValveDto {


    private Long id;

    private String name;

    private String location;


    private String model;


    private Boolean status;
}
