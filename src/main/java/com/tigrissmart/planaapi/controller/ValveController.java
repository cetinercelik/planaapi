package com.tigrissmart.planaapi.controller;

import com.tigrissmart.planaapi.dto.ValveDto;
import com.tigrissmart.planaapi.service.impl.ValveServiceImpl;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@Component
@RequestMapping("/api/valves")
@Slf4j
@CrossOrigin
@NoArgsConstructor
@AllArgsConstructor
public class ValveController {

    @Autowired
    private ValveServiceImpl valveService;

    @GetMapping()
    public ResponseEntity<List<ValveDto>> getAll() {
        List<ValveDto> data = valveService.getAll();
        return ResponseEntity.ok(data);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ValveDto> getById(@PathVariable(value = "id", required = true) Long id) {
        try {
            ValveDto valveDto = valveService.getById(id);
            return ResponseEntity.ok(valveDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping()
    public ResponseEntity<ValveDto> createValve(@Valid @RequestBody ValveDto valveDto) {
        try {
            ValveDto newValve = valveService.save(valveDto);
            return ResponseEntity.created(new URI("api/valves/" + newValve.getId())).body(newValve);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ValveDto> updateValve(@PathVariable(value = "id", required = true) Long id, @Valid @RequestBody ValveDto valveDto) {
        try {
            valveService.update(id, valveDto);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable(value = "id", required = true) Long id) {
        try {
            if (id != null) {
                valveService.delete(id);
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

}